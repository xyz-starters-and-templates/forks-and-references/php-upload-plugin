<?php
App::uses('AppController', 'Controller');
/**
 * Shares Controller
 *
 * @property Share $Share
 * @property PaginatorComponent $Paginator
 * @property SessionComponent $Session
 */
class SharesController extends AppController {


	public function add() {
		if ($this->request->is('post')) {
			$this->Share->create();
			if ($this->Share->save($this->request->data)) {
				$this->Session->setFlash(__('The post has been saved.'));
				return $this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('The post could not be saved. Please, try again.'));
			}
		}
	}


}
