<?php
App::uses('AppController', 'Controller');
/**
 * Votes Controller
 *
 * @property Vote $Vote
 * @property PaginatorComponent $Paginator
 * @property SessionComponent $Session
 */
class VotesController extends AppController {


	public function add() {
		$this->layout='ajax';
		if ($this->request->is('post')) {
			$print_response = true;
			
			if (empty($this->data['Vote']['name'])){
				return $this->generate_response(array("files"=>array(array("message"=>"請輸入姓名!"))),$print_response);
			}			
			if (!preg_match("/^[\x7f-\xff]+$/", $this->data['Vote']['name'])) { 
				return $this->generate_response(array("files"=>array(array("message"=>"請輸入中文!"))),$print_response); 
			} 
			
			if (empty($this->data['Vote']['gender'])){
				return $this->generate_response(array("files"=>array(array("message"=>"請輸入性別!"))),$print_response);
			}
			if (empty($this->data['Vote']['age'])){
				return $this->generate_response(array("files"=>array(array("message"=>"請輸入年齡!"))),$print_response);
			}
			if (!is_numeric($this->data['Vote']['age'])){
				return $this->generate_response(array("files"=>array(array("message"=>"年齡請輸入數字!"))),$print_response);
			}
			
			if (strlen($this->data['Vote']['name'])>=16) { 
				return $this->generate_response(array("files"=>array(array("message"=>"請輸入最多五碼中文!"))),$print_response); 
			}
			
			if ($this->data['Vote']['age']>=100){
				return $this->generate_response(array("files"=>array(array("message"=>"年齡請輸入2碼數字!"))),$print_response);
			}		
					
			if (empty($this->data['Vote']['mobile'])){
				return $this->generate_response(array("files"=>array(array("message"=>"請輸入手機號碼!"))),$print_response);
			}
			if (!is_numeric($this->data['Vote']['mobile'])){
				return $this->generate_response(array("files"=>array(array("message"=>"手機號碼請輸入數字!"))),$print_response);
			}
			if (empty($this->data['Vote']['receipt_1'])){
				return $this->generate_response(array("files"=>array(array("message"=>"請輸入發票編號!"))),$print_response);
			}			
			if (strlen($this->data['Vote']['receipt_1'])!=2 || preg_match('/[^A-Za-z]/', $this->data['Vote']['receipt_1'])){
				return $this->generate_response(array("files"=>array(array("message"=>"請輸入2碼英文編號!"))),$print_response);
			}
			if (empty($this->data['Vote']['receipt_2'])){
				return $this->generate_response(array("files"=>array(array("message"=>"請輸入發票編號!"))),$print_response);
			}
			if (!is_numeric($this->data['Vote']['receipt_2'])){
				return $this->generate_response(array("files"=>array(array("message"=>"請輸入數字發票編號!"))),$print_response);
			}
			
			if (empty($this->data['Vote']['product1']) && empty($this->data['Vote']['product2']) && empty($this->data['Vote']['product3']) && empty($this->data['Vote']['product4'])){
				return $this->generate_response(array("files"=>array(array("message"=>"請輸購買品項!"))),$print_response);
			}	
			if (empty($this->data['Vote']['shop'])){
				return $this->generate_response(array("files"=>array(array("message"=>"請輸入實體/網路通路!"))),$print_response);
			}
			if (empty($this->data['Vote']['agreeread']) || empty($this->data['Vote']['agreecheck'])){
				return $this->generate_response(array("files"=>array(array("message"=>"請確認已讀活動辦法及注意事項說明!"))),$print_response);
			}
			
			return $this->generate_response(array("files"=>array(array("message"=>"活動已結束!"))),$print_response);
			
			
			$data = $this->data;
			$data['Vote']['round2']=1;

		
			$this->Vote->create();
			if ($this->Vote->save($data)) {
				return $this->generate_response(array("files"=>array(array("status"=>1))),$print_response);	
			} else {
					
			}
			
		}
	}

	protected function generate_response($content, $print_response = true) {
        if ($print_response) {
            $json = json_encode($content);
            $redirect = isset($_REQUEST['redirect']) ?
                stripslashes($_REQUEST['redirect']) : null;
            if ($redirect) {
                $this->header('Location: '.sprintf($redirect, rawurlencode($json)));
                return;
            }
            $this->head();
            if (isset($_SERVER['HTTP_CONTENT_RANGE'])) {
                $files = isset($content[$this->options['param_name']]) ?
                    $content[$this->options['param_name']] : null;
                if ($files && is_array($files) && is_object($files[0]) && $files[0]->size) {
                    $this->header('Range: 0-'.($this->fix_integer_overflow(intval($files[0]->size)) - 1));
                }
            }
            $this->body($json);
        }
        return $content;
    }
	
	protected function send_content_type_header() {
        $this->header('Vary: Accept');
        if (isset($_SERVER['HTTP_ACCEPT']) &&
            (strpos($_SERVER['HTTP_ACCEPT'], 'application/json') !== false)) {
            $this->header('Content-type: application/json');
        } else {
            $this->header('Content-type: text/plain');
        }
    }
	
	public function head() {
        $this->header('Pragma: no-cache');
        $this->header('Cache-Control: no-store, no-cache, must-revalidate');
        $this->header('Content-Disposition: inline; filename="files.json"');
        // Prevent Internet Explorer from MIME-sniffing the content-type:
        $this->header('X-Content-Type-Options: nosniff');
        $this->header('Access-Control-Allow-Origin: *');
        $this->header('Access-Control-Allow-Credentials: false');
		$options = array('access_control_allow_methods' => array(
                'OPTIONS',
                'HEAD',
                'GET',
                'POST',
                'PUT',
                'PATCH',
                'DELETE'
            ),
            'access_control_allow_headers' => array(
                'Content-Type',
                'Content-Range',
                'Content-Disposition'
            ));
        $this->header('Access-Control-Allow-Methods: '
            .implode(', ', $options['access_control_allow_methods']));
        $this->header('Access-Control-Allow-Headers: '
            .implode(', ', $options['access_control_allow_headers']));
        $this->send_content_type_header();
    }
	
	protected function body($str) {
        echo $str;
    }

}
