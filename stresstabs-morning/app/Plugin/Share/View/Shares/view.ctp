<style>
<!--
.form-horizontal .control-label {
	padding-top: 0px;
}
-->
</style>
<div class="container">
	<h2>
		<?php echo __('Share Manager: Share (view)'); ?>
	</h2>
	<div class="row-fluid show-grid" id="tab_user_manager">
		<div class="span12">
			<ul class="nav nav-tabs">
				<?php if ($this->Acl->check('Categories','index','Share') == true){?>
					<li><?php echo $this->Html->link(__('Category'), array('plugin' => 'share','controller' => 'categories','action' => 'index')); ?></li>
				<?php } ?>
				<?php if ($this->Acl->check('Shares','index','Share') == true){?>
					<li class="active"><?php echo $this->Html->link(__('Share'), array('plugin' => 'share','controller' => 'shares','action' => 'index')); ?></li>
				<?php }?>
			</ul>
		</div>
	</div>
	<div class="row-fluid show-grid">
		<div class="span12">
			<form class="form-horizontal">
				<div class="control-group">
					<label class="control-label"><?php echo __('Id'); ?>
					</label>
					<div class="controls">
						<?php echo h($share['Share']['id']); ?>
					</div>
				</div>
				<div class="control-group">
					<label class="control-label"><?php echo __('Share Title'); ?>
					</label>
					<div class="controls">
						<?php echo h($share['Share']['share_title']); ?>
					</div>
				</div>
				<div class="control-group">
					<label class="control-label"><?php echo __('Category'); ?>
					</label>
					<div class="controls">
						<?php echo $share['Category']['category_name']; ?>
					</div>
				</div>
				<div class="control-group">
					<label class="control-label"><?php echo __('Share Date'); ?>
					</label>
					<div class="controls">
						<?php echo h($share['Share']['share_date']); ?>
					</div>
				</div>

				<div class="control-group">
					<label class="control-label"><?php echo __('Share Summary'); ?>
					</label>
					<div class="controls">
						<?php echo h($share['Share']['share_summary']); ?>
					</div>
				</div>

				<div class="control-group">
					<label class="control-label"><?php echo __('Share Content'); ?>
					</label>
					<div class="controls">
						<?php echo $share['Share']['share_content']; ?>
					</div>
				</div>
				<div class="form-actions">
					<?php echo $this->Acl->link(__('Edit'), array('action' => 'edit', $share['Share']['id']),array('class'=>'btn  btn-primary')); ?>
					<a class="btn " onclick="cancel_add();"><?php echo __('Cancel'); ?>
					</a>
				</div>
			</form>
		</div>
	</div>
</div>
<script>
	function cancel_add() {
		window.location = '<?php echo Router::url(array('plugin' => 'share','controller' => 'shares','action' => 'index')); ?>';
	}
</script>
