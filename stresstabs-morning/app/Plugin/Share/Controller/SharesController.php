<?php
App::uses('ShareAppController', 'Share.Controller');
/**
 * Shares Controller
 *
 * @property Share $Share
*/
class SharesController extends ShareAppController {

	/**
	 * index method
	 *
	 * @return void
	 */
	public function index() {
		if ($this->request->isAjax()){
			$this->layout = null;
		}
		$this->Share->recursive = 0;
		
		if ($this->request->isGet()){
			if (!empty($this->request->named['filter'])){
				$filter = array();
				$filter['Share']['filter'] = $this->request->named['filter'];
				if (!empty($this->request->params['named']['page'])){
					$filter['Share']['page'] = $this->request->named['page'];
				}else{
					$filter['Share']['page'] = 1;
				}
				$this->request->data = am($this->request->data,$filter);
			}else{
				$filter = array();
				$filter['Share'] = $this->Cookie->read('srcPassArg');
				if (!empty($filter['Share'])){
					$this->request->data = am($this->request->data,$filter);
				}
			}
		}
		
		
		$passArg = array();
		$conditions = array();
		if (!empty($this->data['Share']) && !empty($this->data['Share']['filter'])){
			$condition = array(' title LIKE '  => '%'.trim($this->data['Share']['filter']).'%');
			$passArg = $this->data['Share'];
			array_push($conditions,$condition);
		}
		if (!empty($this->request->params['named']['page'])){
			$passArg['page'] = 1;
		}else{
			if (!empty($this->request->data['Share']['page'])){
				$this->request->params['named']['page'] = $this->request->data['Share']['page'];
			}
		}
		
		//$paginate = array('limit' => 2);
		$paginate = array();
		if ($this->Session->read('auth_user')['Group'][0]['id']!=1){
			$condition = array('owner_id'=>$this->Session->read('auth_user')['User']['id']);
			array_push($conditions,$condition);
		}
		if (!empty($conditions)){
			$paginate['conditions'] = $conditions;
		}
		
		//print_r($this->data);
		
		//$paginate['order']="Share.ordering DESC";
		
		$this->paginate = $paginate;
		
		$this->set('passArg',$passArg);
		
		if (!empty($passArg)){
			$this->Cookie->write('srcPassArg',$passArg);
		}
		
		$this->set('shares', $this->paginate());		
		
	}

	/**
	 * view method
	 *
	 * @throws NotFoundException
	 * @param string $id
	 * @return void
	 */
	public function view($id = null) {
		$this->Share->id = $id;
		if (!$this->Share->exists()) {
			throw new NotFoundException(__('Invalid share'));
		}
		$this->set('share', $this->Share->read(null, $id));
	}

	/**
	 * add method
	 *
	 * @return void
	 */
	public function add() {
		$errors = array();
		if ($this->request->is('post')) {
			$this->Share->create();
			
			$user = $this->Session->read('Auth');
			$this->request->data['Share']['owner_id']=$user['User']['id'];
			
			if ($this->Share->save($this->request->data)) {
				$this->Cookie->delete('srcPassArg');
				$this->redirect(array('action' => 'edit',$this->Share->id));
			} else {
				$errors = $this->Share->validationErrors;
			}
		}
		
		$this->set('errors',$errors);
		$this->set(compact('categories'));
	}

	/**
	 * edit method
	 *
	 * @throws NotFoundException
	 * @param string $id
	 * @return void
	 */
	public function edit($id = null) {
		$errors = array();
		$this->Share->id = $id;
		if (!$this->Share->exists()) {
			throw new NotFoundException(__('Invalid share'));
		}
		if ($this->request->is('post') || $this->request->is('put')) {
			if ($this->Share->save($this->request->data)) {
				$this->redirect(array('action' => 'index'));
			} else {
				$errors = $this->Share->validationErrors;
			}
		} else {
			$share = $this->Share->read(null, $id);
			if ($share['Share']['owner_id']!=$this->Session->read('auth_user')['User']['id'] && $this->Session->read('auth_user')['Group'][0]['id']!=1)
				$this->redirect(array('action'=>'index')); 
			$this->request->data = am($this->request->data,$share);
		}
		$this->Session->write("CurrentPID",$id);
		$this->set('errors',$errors);
		$this->set(compact('categories'));
	}

	/**
	 * delete method
	 *
	 * @throws MethodNotAllowedException
	 * @throws NotFoundException
	 * @param string $id
	 * @return void
	 */
	public function delete($id = null) {
		$this->autoRender = false;
		if (!$this->request->is('post')) {
			throw new MethodNotAllowedException();
		}
		$this->Share->id = $id;
		if (!$this->Share->exists()) {
			throw new NotFoundException(__('Invalid share'));
		}
		if ($this->Share->delete()) {
			//$this->Session->setFlash(__('Share deleted'));
			//$this->redirect(array('action' => 'index'));
		}
		//$this->Session->setFlash(__('Share was not deleted'));
		//$this->redirect(array('action' => 'index'));
	}
}
