var questions = [{id:0,q:"請問以下哪一款是近期在日系車款中，最暢銷的Bosch火星塞?",a:0,a0:"a. 雙銥合金火星塞",a1:"b. 白金火星塞",a2:"c. 銅釔火星塞"},{id:1,q:"請問Bosch新上市的日系剎車片中，有哪些特色?",a:2,a0:"a. 陶瓷配",a1:"b. 制動力佳﹑低噪音﹑耐磨耗",a2:"c. 以上皆是"},{id:2,q:"請問Bosch通用軟骨雨刷-旗艦型，有哪些特色?",a:2,a0:"a. 非對稱導流板的設計，在高速行駛下，更貼和玻璃",a1:"b. 三層橡膠的設計，可以更穩固耐用",a2:"c. 以上皆是"},{id:3,q:"請問Bosch二代銀合金電瓶，可以符合車輛的哪些需求?",a:2,a0:"a. 因應AMS需求的高電量，以及車輛中電子設備的高電能。",a1:"b. 超高的蓄電力，使用壽命長",a2:"c. 以上皆是"},{id:4,q:"請問Bosch全效型汽車冷氣濾網，除了抗菌﹑除臭﹑除塵外，還有多了哪些功效??",a:2,a0:"a. 抑制過敏物質﹑抗病毒",a1:"b. 防霉",a2:"c. 以上皆是"},{id:5,q:"請問Bosch汽車零件可以適用於以下哪些車款?",a:2,a0:"a. 歐美車款",a1:"b. 日韓車款",a2:"c. 以上皆是"}];
		var answered = 0, score = 0, current = Math.floor(Math.random() * questions.length - 1);
			
			
			function displayQuestion(number){
				$(".question").html(questions[number].q);
				$("#a0").html(questions[number].a0);
				$("#a1").html(questions[number].a1);
				$("#a2").html(questions[number].a2);
				$("#counter").html((answered+1).toString()+"/3");				
			}
			
			function displayNextQuestion() {
				num = Math.floor(Math.random() * questions.length);
			    while (num==current)
			    	num = Math.floor(Math.random() * questions.length);
			    current = num;
			    //console.log(num);
			    resetTimer();			    
			    displayQuestion(num);			    
			}
			function answerQuestion(answer){
				if (answered<3){
					if (questions[current].a==answer){
						score++;
						$('#correct').modal('show') ;
					}else{
						switch(questions[current].a) {
						    case 0:
						        $("#ans").html('a');
						        break;
						    case 1:
						        $("#ans").html('b');
						        break;
					        case 2:
						        $("#ans").html('c');
						        break;
						    default:
						        $("#ans").html('a');
						}
						$('#wrong').modal('show') ;
					}					
				}
				answered++;
				
				window.clearInterval(intervals.main);
				for (var elem = 0; elem<intervals.length;elem++)
					if (intervals[elem])
    					window.clearInterval(intervals[elem]);
				digits = [];
				intervals = [];
				
				
				
			}
			
			function proceed(){				
				if (answered==3){
					alert("game over!");					
					window.clearInterval(intervals.main);
					for (var elem = 0; elem<intervals.length;elem++)
						if (intervals[elem])
	    					window.clearInterval(intervals[elem]);
					digits = [];
					intervals = [];
					
					$("#digits").html('');
					$("#digits").removeData();
					$("#digits").attr('started', false);
					
					$("#digits").countdown({
			          image: "img/digits.png",
			          format: "mm:ss",
			          endTime: new Date(),
			          start:false		          
			        });
			        
				}else{
					questions.splice(current,1);
					displayNextQuestion();
				}
			}
			
			function resetTimer(){
				window.clearInterval(intervals.main);
				for (var elem = 0; elem<intervals.length;elem++)
					if (intervals[elem])
    					window.clearInterval(intervals[elem]);
				digits = [];
				intervals = [];
				
				$("#digits").html('');
				$("#digits").removeData();
				$("#digits").attr('started', false);
				
				var t = new Date();
				t.setSeconds(t.getSeconds() + 15);
	
		        $("#digits").countdown({
		          image: "img/digits.png",
		          format: "mm:ss",
		          endTime: t,		          
    				timerEnd: function() { $('#timesup').modal('show') ;},
		        });
			}	
     	$(function(){
     		displayNextQuestion();
     	});